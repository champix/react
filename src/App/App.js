import React from 'react';

import logo from './logo.svg';
import './App.css';
import './bg.jpg';

import SearchComponent from './components/SearchComponent/SearchComponent';

class App extends React.Component{
 
  constructor(props){
    super(props);

    this.state={
      pageType:'Hello',
    };
  }

  render(){

     return  (
        <div className="App">

          <header className={"App-header "+  this.state.pageType === 'book'?' hide':''}>

          <SearchComponent></SearchComponent>
            
          <img src={logo} className="App-logo" alt="logo" />

          <SearchComponent onSelectBook={ () => false}/>

          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >Learn React</a>

          </header>

          </div>
       );
     }
}

export default App;
