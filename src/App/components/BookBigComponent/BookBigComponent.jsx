import React from 'react';

function BookComponent(props){
    const src =  ( props.item.imgsrc && props.item.imgsrc ) || 'img/defaultbook.jpg';

    return (<div className="big-book book"><img src={ src }/><br/>A Short book</div>);
}
BookComponent.defaultProps = {}

export default BookComponent;
