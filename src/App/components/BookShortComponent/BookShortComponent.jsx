import React from 'react';

function ShortBookComponent(props){

    const small = props.extrasmall && 'search-result-list ';
    const itemSrc = ( props.item.imgsrc && props.item.imgsrc ) || 'img/defaultbook.jpg'

    return (
        <div className={ small+"short-book book" }>
            <img className="short-book-img" src={ itemSrc }/>
            <div className="short-book-description book-description">
                <big><u><i>{ props.item.title }</i></u></big>
                <p>
                    Synopsys : ***
                </p>
            </div> <hr />
        </div>
        );
}
ShortBookComponent.defaultProps = {}

export default ShortBookComponent;
