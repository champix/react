import React from 'react';
import BookShortComponent from '../BookShortComponent/BookShortComponent';
import './SearchComponent.css'

function Results (props){

    const RendererComponent = props.children.type;

    return (
        <div className={'search-results ' + props.hidden?'shown':'hide'}>
            <ul>
                {props.items && props.items.map((item,index)=>{
                    return <li><RendererComponent item={item} key={index}/></li>;
                })}
            </ul>
        </div>
    );
}

class SearchComponent extends React.Component{
    
    constructor(props){
        super(props);

        this.state={
            searchText:''
        }
    }

    searchChangeHandler = (evt) => {        
        this.setState({ searchText:evt.target.value });
    };

    render(){

       return(
            <div className={'search-component '+this.state.position}>
                
                <input type="text" 
                name="searchInput"
                value={this.state.searchText}
                onChange={this.searchChangeHandler} placeholder="Recherche"/>

                {
                  this.state.searchStr && 
                  this.props.results.lenght && 
                  <Results items={this.props.results}>
                    <BookShortComponent/>
                  </Results>  
                }

            </div>
        );
    }
}

export default SearchComponent;

