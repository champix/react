
class BookService {

  static get content(){
    return [
      {imgsrc:'img/martine1.jpg',title:'Martine 1'},
      {imgsrc:'img/martine2.jpg',title:'Martine 2'},
      {imgsrc:'img/martine3.jpg',title:'Martine 3'},
      {imgsrc:'img/martine4.jpg',title:'Martine 4'},
      {imgsrc:'img/martine5.jpg',title:'Martine 5'},
      {imgsrc:'img/martine6.jpg',title:'Martine 6'}
      ];
  }

  static fetchServerData(){

    return Promise.resolve(this.content);

    // return fetch('API_URL').then(res => res.json())
  }

  static searchServerData( text ){
    
    return Promise.resolve(this.content.filter( item => (new RegExp(text,'g')).test(item.title)));

    // return fetch('API_URL').then(res => res.json())
  }

}